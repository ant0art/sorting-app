package com.epam;

import java.util.*;

public class SortingApp {
	
	public static void main(String[] args) {
		System.out.println(
				"Enter numbers for the sorting (\"exit\" word ends the " + "entering):");
		Scanner scanner = new Scanner(System.in);
		List<Integer> integerList = new ArrayList<>();
		while (scanner.hasNext()) {
			String next = scanner.next();
			if (next.equals("exit")) break;
			try {
				integerList.add(Integer.parseInt(next));
			} catch (NumberFormatException e) {
				System.out.println(e.getMessage());
			}
		}
		new SortingApp().sort(integerList);
		System.out.println(integerList);
	}
	
	public void sort(List<Integer> list) {
		if (list == null) throw new NullPointerException();
		if (list.size() > 1) {
			for (int i = list.size() - 1; i >= 1; i--) {
				for (int j = 0; j < i; j++) {
					if (list.get(j) > list.get(j + 1)) {
						int temp = list.get(j);
						list.set(j, list.get(j + 1));
						list.set(j + 1, temp);
					}
				}
			}
		}
	}
}
