package com.epam;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import junit.framework.*;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class SortingAppTenArgumentsTest extends TestCase {
	
	private final List<Integer> integerList;
	SortingApp sortingApp = new SortingApp();
	
	public SortingAppTenArgumentsTest(List<Integer> integerList) {
		this.integerList = integerList;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> data1() {
		return Arrays.asList(new Object[][]{
				{Arrays.asList(1, 2, 3, 4, 5, 6, 10, 9, 8, 7)},
				{Arrays.asList(-1, -2, -3, -4, -5, -6, -10, -9, -8, -7)},
				{Arrays.asList(-1, 2, -3, 4, -5, 6, -10, 9, -8, 7)},
				{Arrays.asList(1, -2, 3, -4, 5, -6, 10, -9, 8, -7)}});
	}
	
	@org.junit.Test
	public void sort() {
		System.out.println("Suite SortingAppTenArgumentsTest is executing");
		
		List<Integer> testList = integerList;
		testList.sort(Comparator.comparingInt(x -> x));
		sortingApp.sort(integerList);
		assertArrayEquals(testList.toArray(new Integer[0]),
				integerList.toArray(new Integer[0]));
	}
}
