package com.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;
import junit.framework.*;

import static org.junit.Assert.assertArrayEquals;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class SortingAppZeroAndOneArgumentsTest extends TestCase {
	
	private final List<Integer> integerList;
	SortingApp sortingApp = new SortingApp();
	
	public SortingAppZeroAndOneArgumentsTest(List<Integer> integerList) {
		this.integerList = integerList;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> data1() {
		return Arrays.asList(new Object[][]{
				{Collections.emptyList()}, {Collections.singletonList(1)},
				{Collections.singletonList(0)}, {Collections.singletonList(-3)}});
	}
	
	@Test
	public void sort() {
		System.out.println("Suite SortingAppZeroAndOneArgumentsTest is executing");
		List<Integer> expectedList = integerList;
		sortingApp.sort(integerList);
		assertArrayEquals("The list should not be changed",
				expectedList.toArray(new Integer[0]),
				integerList.toArray(new Integer[0]));
	}
}
