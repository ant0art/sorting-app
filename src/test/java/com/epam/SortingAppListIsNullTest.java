package com.epam;

import org.junit.Test;

/**
 * Unit test for simple App.
 */

public class SortingAppListIsNullTest {
	
	SortingApp sortingApp = new SortingApp();
	
	public SortingAppListIsNullTest() {
	
	}
	
	@Test(expected = NullPointerException.class)
	public void sort() {
		System.out.println("Suite SortingAppListIsNullTest is executing");
		
		sortingApp.sort(null);
	}
}
