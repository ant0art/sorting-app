package com.epam;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import junit.framework.*;

/**
 * Unit test for simple App.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
		SortingAppListIsNullTest.class, SortingAppZeroAndOneArgumentsTest.class,
		SortingAppTenArgumentsTest.class, SortingAppMoreThanTenArgumentsTest.class})
public class SortingAppTest extends TestCase {
	
	public SortingAppTest() {
	}
	
	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(SortingAppTest.class);
	}
	
	/**
	 * Rigourous Test :-)
	 */
	public void testSortingApp() {
		assertTrue(true);
	}
}
